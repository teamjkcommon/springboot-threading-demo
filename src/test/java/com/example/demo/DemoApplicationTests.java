package com.example.demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {

	@Test
	public void testSingleThreadApp() {
		SingleThreadedEventHandler app = new SingleThreadedEventHandler();
		app.subscribe(new Subscriber());
		app.subscribe(new Subscriber());
		app.subscribe(new Subscriber());
		app.subscribe(new Subscriber());
		app.subscribe(new Subscriber());
		app.subscribe(new Subscriber());
		app.runApp();

		assert true;
	}

	@Test
	public void testMultiThreadApp() {
		MultiThreadedEventHandler app = new MultiThreadedEventHandler();
		app.subscribe(new Subscriber());
		app.subscribe(new Subscriber());
		app.subscribe(new Subscriber());
		app.subscribe(new Subscriber());
		app.subscribe(new Subscriber());
		app.subscribe(new Subscriber());
		app.runApp();

		assert true;
	}

}
