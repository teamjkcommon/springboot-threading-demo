package com.example.demo;

import java.util.ArrayList;
import java.util.List;

public class SingleThreadedEventHandler {

    private List<Subscriber> subscriberList = new ArrayList<>();

    public void subscribe(Subscriber subscriber) {
        if (subscriberList.contains(subscriber)) {
            return;
        }

        subscriberList.add(subscriber);
    }

    public void runApp() {
        Event event = new Event();

        for (Subscriber subscriber : subscriberList) {
            // We emit the event to all subscribers. The problem
            // with the single thread environment is that we have
            // to wait for a subscriber to finish handling the
            // event before we can emit the event to the next subscriber.
            // We also have to wrap the method in a try catch so that this
            // framework does not fail when the subscriber throws an exception.

            // A fix for this would be to ask developers of this app to
            // handle events in a seperate thread so that this method
            // returns fast - and so we can very fast emid the event to
            // all subscribers.
            // The other fix is to do this on behalf of the developer of this
            // app by running a multithreaded environment.
            try {
                subscriber.onEvent(event);
            } catch (Throwable t) {
                System.out.println("Ignoring..");
            }
        }

    }

}
