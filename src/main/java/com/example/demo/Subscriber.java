package com.example.demo;

public class Subscriber implements EventHandler {

    @java.lang.Override
    public void onEvent(Event event) {
        System.out.println("Handling event: " + event);
    }
}
