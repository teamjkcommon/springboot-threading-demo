package com.example.demo;

public interface EventHandler {

    void onEvent(Event event);
}
