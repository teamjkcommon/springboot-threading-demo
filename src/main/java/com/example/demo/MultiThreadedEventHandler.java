package com.example.demo;

import java.util.ArrayList;
import java.util.List;

public class MultiThreadedEventHandler {

    private List<Subscriber> subscriberList = new ArrayList<>();

    public void subscribe(Subscriber subscriber) {
        if (subscriberList.contains(subscriber)) {
            return;
        }

        subscriberList.add(subscriber);
    }

    public void runApp() {
        Event event = new Event();

        for (Subscriber subscriber : subscriberList) {
            // We emit the event to all subscribers. We spin a new
            // thread for each subscriber so that we're isolated from
            // the subscriber. Any exceptions thrown by the subscriber
            // will not be caught and ignored.

            // An improvement to this solution is to create a Thread pool
            // so emitting goes even faster. Saves on garbage collection.
            new Thread() {
                public void run() {
                    subscriber.onEvent(event);
                }
            }.start();

        }

    }
}
